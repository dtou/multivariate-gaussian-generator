#ifndef GIBBSSAMPLER_CPP
#define GIBBSSAMPLER_CPP

#include "TRandom3.h"
#include "GibbsSampler.hpp"
#include <math.h>
#include <numeric>

TRandom3 GibbsSampler::m_generator = TRandom3();

GibbsSampler::GibbsSampler(){
}

GibbsSampler::GibbsSampler(const std::vector <double>& _means, const TMatrixDSym& _covarianceMatrix)
    : MultiVarGaussianSampler(_means, _covarianceMatrix) {
    StoreMultiVarGaussian(_means, _covarianceMatrix);
    CacheDeviations();
    GetInitialState();
    RunBurnInCycles();
}

void GibbsSampler::CacheDeviations() {
    m_currentDeviations.resize(m_nDimensions); // This is only used while calculating conditional means
    m_standardDeviations.resize(m_nDimensions);
    auto _calculateStandardDeviation = [this](double a){ return sqrt(1./a);};
    std::transform(m_diagonal.begin(), m_diagonal.end(), m_standardDeviations.begin(), _calculateStandardDeviation);
}

void GibbsSampler::GetInitialState() {
    m_previousState.resize(m_nDimensions);
    for (int _index = 0; _index < m_nDimensions; _index++) {
        m_previousState[_index] = m_generator.Gaus(m_means[_index], m_standardDeviations[_index]);
    }
}

void GibbsSampler::RunBurnInCycles() {
    for (int i = 0; i < m_burnIn; i++) {
        SampleOnce();
    }
}

void GibbsSampler::SampleOnce() {
    std::vector <double> _currentState = m_previousState;
    for (int _index = 0; _index < m_nDimensions; _index++) {
        double _conditionalMean = GetConditionalMean(_currentState, _index);
        double _conditionalDeviation = m_standardDeviations[_index];
        _currentState[_index] = m_generator.Gaus(_conditionalMean, _conditionalDeviation);
    }
    m_previousState = _currentState;
}

std::vector <double> GibbsSampler::Sample() {
    for (int i = 0; i < m_nSkip; i++) {
        // Since there is correlation between samples of MCMC, we skip a number of samples before returning
        SampleOnce();
    }
    return m_previousState;
}

double GibbsSampler::GetConditionalMean(std::vector <double> & _currentState, const int _index) {
    double _variance = m_standardDeviations[_index] * m_standardDeviations[_index];
    std::transform(m_means.begin(), m_means.end(), _currentState.begin(), m_currentDeviations.begin(), std::minus<double>());
    double _dotProduct = std::inner_product(m_invertedCovariance[_index].begin(), m_invertedCovariance[_index].end(), m_currentDeviations.begin(), 0.);
    double _conditionalMean = _variance * ( _dotProduct + m_diagonal[_index] * _currentState[_index]);
    return _conditionalMean;
}

void GibbsSampler::SetDimensions(const std::vector <double> & _means) {
    m_nDimensions = _means.size();
}

void GibbsSampler::StoreMeans(const std::vector <double> & _means) {
    m_means = _means;
}

void GibbsSampler::StoreInvertedCovariance(const TMatrixDSym& _invertedMatrix){
    AllocateMatrixMemory();
    StoreMatrixElements(_invertedMatrix);
    StoreDiagonalElements(_invertedMatrix);
}

void GibbsSampler::AllocateMatrixMemory() {
    auto _needMallocMemory = m_nDimensions * sizeof(std::vector <double>);
    m_invertedCovariance = (std::vector <double> *)malloc(_needMallocMemory);
    for (int _row = 0; _row < m_nDimensions; _row++){
        m_invertedCovariance[_row].resize(m_nDimensions);
    }
}

void GibbsSampler::StoreMatrixElements(const TMatrixDSym& _invertedMatrix) {
    //Store the matrix as row major to optimise memory access
    for (int _row = 0; _row < m_nDimensions; _row++) {
        for (int _column = 0; _column < m_nDimensions; _column++) {
            m_invertedCovariance[_row][_column] = _invertedMatrix(_row, _column);
        }
    }
}

void GibbsSampler::StoreDiagonalElements(const TMatrixDSym& _invertedMatrix) {
    m_diagonal.resize(m_nDimensions);
    for (int _index = 0; _index < m_nDimensions; _index++){
        m_diagonal[_index] = _invertedMatrix(_index, _index);
    }
}

#endif