#ifndef MULTIVARGAUSSIANSAMPLER_CPP
#define MULTIVARGAUSSIANSAMPLER_CPP

#include "MultiVarGaussianSampler.hpp"
#include "TDecompBK.h"

MultiVarGaussianSampler::MultiVarGaussianSampler() {
}

MultiVarGaussianSampler::MultiVarGaussianSampler(const std::vector <double> & _means, const TMatrixDSym& _covarianceMatrix) {
    ThrowIfDimensionDoesNotAgree(_means, _covarianceMatrix);
}

void MultiVarGaussianSampler::StoreMultiVarGaussian(const std::vector <double> & _means, const TMatrixDSym& _covarianceMatrix) {
    TDecompBK BunchKaufmanDecomposition(_covarianceMatrix);
    auto _invertedCovariance = BunchKaufmanDecomposition.Invert();
    SetDimensions(_means);
    StoreMeans(_means);
    StoreInvertedCovariance(_invertedCovariance);
}

void MultiVarGaussianSampler::ThrowIfDimensionDoesNotAgree(const std::vector <double> & _means, const TMatrixDSym& _covarianceMatrix) const {
    int _nDimMeans = _means.size();
    int _nDimMatrix = _covarianceMatrix.GetNrows();
    if (_nDimMeans != _nDimMatrix){
        throw DimensionError(std::to_string(_nDimMeans), std::to_string(_nDimMatrix));
    }
}

#endif
