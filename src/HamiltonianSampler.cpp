#ifndef HAMILTONIANSAMPLER_CPP
#define HAMILTONIANSAMPLER_CPP

#include "HamiltonianSampler.hpp"
#include <numeric>
#include <iostream>

TRandom3 HamiltonianSampler::m_numberGenerator = TRandom3(0);

HamiltonianSampler::HamiltonianSampler(){
}

HamiltonianSampler::HamiltonianSampler(const std::vector <double> _means, const TMatrixDSym& _covarianceMatrix)
    : MultiVarGaussianSampler(_means, _covarianceMatrix) {
    StoreMultiVarGaussian(_means, _covarianceMatrix);
    StoreErrors(_covarianceMatrix);
    AllocateMemory();
    GetInitialState();
    RunBurnInCycles();
}

void HamiltonianSampler::StoreErrors(const TMatrixDSym& _covarianceMatrix) {
    m_errors.resize(m_nDimensions);
    for (int index = 0; index < m_nDimensions; index++){
        m_errors[index] = sqrt(_covarianceMatrix(index, index));
    }
}

void HamiltonianSampler::AllocateMemory() {
    m_previousPosition.resize(m_nDimensions);
    m_currentPosition.resize(m_nDimensions);
    m_previousMomentum.resize(m_nDimensions);
    m_currentMomentum.resize(m_nDimensions);
    m_differentialPotential.resize(m_nDimensions);
    m_positionOffset.resize(m_nDimensions);
}

void HamiltonianSampler::GetInitialState() {
    for (int i = 0; i < m_nDimensions; i++){
        m_previousMomentum[i] = m_numberGenerator.Gaus();
        m_previousPosition[i] = m_numberGenerator.Gaus(m_means[i], m_errors[i]);
    }
    CalculatePositionOffset();
    CalculateDifferentialPotential();
    m_previousHamiltonian = GetCurrentHamiltonian();
}

void HamiltonianSampler::CalculatePositionOffset() {
    for (int i = 0; i < m_nDimensions; i++) {
        m_positionOffset[i] = m_currentPosition[i] - m_means[i];
    }
}

void HamiltonianSampler::CalculateDifferentialPotential() {
    for (int i = 0; i < m_nDimensions; i++) {
        m_differentialPotential[i] = 0;
        for (int j = 0; j < m_nDimensions; j++) {
            m_differentialPotential[i] += m_invertedCovariance[i][j] * m_positionOffset[j];
        }
    }
}

double HamiltonianSampler::GetCurrentHamiltonian() {
    double _kineticEnergy = GetCurrentKineticEnergy();
    double _potentialEnergy = GetCurrentPotentialEnergy();
    double _hamiltonian = _kineticEnergy + _potentialEnergy;
    return _hamiltonian;
}

double HamiltonianSampler::GetCurrentKineticEnergy() {
    double _kineticEnergy = 0.5 * std::inner_product(m_currentMomentum.begin(), m_currentMomentum.end(), m_currentMomentum.begin(), 0.);
    return _kineticEnergy;
}

double HamiltonianSampler::GetCurrentPotentialEnergy() {
    // Potential energy is tensor multiplication 0.5*(x-mean)*(invertedCovariance)*(x-mean)
    // Differential of potential w.r.t. x is (invertedCovariance)*(x-mean)
    double _potentialEnergy = 0.5 * std::inner_product( m_positionOffset.begin(), m_positionOffset.end() , m_differentialPotential.begin(), 0.);
    return _potentialEnergy;
}

void HamiltonianSampler::RunBurnInCycles() {
    for (int i = 0; i < m_burnInCycles; i++) {
        Sample();
    }
}

std::vector <double> HamiltonianSampler::Sample(){
    double _threshold, _acceptanceProbability;
    do {
        _threshold = m_numberGenerator.Uniform();
        _acceptanceProbability = SampleOnce();
    }
    while (_threshold > _acceptanceProbability); // Reject with probability
    // Update the states once we have a sample which is accepted
    m_previousMomentum = m_currentMomentum;
    m_previousPosition = m_currentPosition;
    m_previousHamiltonian = m_currentHamiltonian;
    return m_currentPosition;
}

double HamiltonianSampler::SampleOnce() {
    m_currentPosition = m_previousPosition;
    ProposeNewMomentum();
    MomentumLeap(LeapLength::HalfLeap);
    PositionLeap();
    for (int i = 0; i < m_leapPerSample; i++) {
        MomentumLeap(LeapLength::FullLeap);
        PositionLeap();
    }
    MomentumLeap(LeapLength::HalfLeap);
    CalculatePositionOffset();
    CalculateDifferentialPotential();
    double _acceptanceProbability = GetAcceptanceProbability();
    return _acceptanceProbability;
}

void HamiltonianSampler::ProposeNewMomentum() {
    for (int i = 0; i < m_nDimensions; i++){
        m_currentMomentum[i] = m_numberGenerator.Gaus();
    }
}

void HamiltonianSampler::MomentumLeap(LeapLength _leapLength) {
    CalculatePositionOffset();
    CalculateDifferentialPotential();
    // We need to apply a scale/normalisation to the potential energy differential to get unit variance momentum
    switch(_leapLength) {
        case (LeapLength::FullLeap) : {
            for (int i = 0; i < m_nDimensions; i++) {
                m_currentMomentum[i] = m_currentMomentum[i] - m_stepSize * m_errors[i] * m_differentialPotential[i];
            }
            break;
        }
        case (LeapLength::HalfLeap) : {
            for (int i = 0; i < m_nDimensions; i++) {
                m_currentMomentum[i] = m_currentMomentum[i] - 0.5 * m_stepSize * m_errors[i] * m_differentialPotential[i];
            }
            break;
        }
    }
}

void HamiltonianSampler::PositionLeap() {
    // In hamiltonian MC literature, m_currentMomentum is the differential of the kinetic energy
    // We need to apply a scale/normalisation to the momentum to have the same magnitude as position
    for (int i = 0; i < m_nDimensions; i++) {
        m_currentPosition[i] = m_currentPosition[i] + m_stepSize * m_errors[i] * m_currentMomentum[i];
    }
}

double HamiltonianSampler::GetAcceptanceProbability() {
    m_currentHamiltonian =  GetCurrentHamiltonian(); 
    double _canonicalDistribution = exp( m_previousHamiltonian - m_currentHamiltonian );
    double _acceptanceProbability = fmin(1., _canonicalDistribution);
    return _acceptanceProbability;
}

void HamiltonianSampler::SetDimensions(const std::vector <double> & _means) {
    m_nDimensions = _means.size();
}

void HamiltonianSampler::StoreMeans(const std::vector <double> & _means) {
    m_means = _means;
}

void HamiltonianSampler::StoreInvertedCovariance(const TMatrixDSym& _invertedMatrix) {
    AllocateMatrixMemory();
    StoreMatrixElements(_invertedMatrix);
}

void HamiltonianSampler::AllocateMatrixMemory() {
    auto _needMallocMemory = m_nDimensions * sizeof(std::vector <double>);
    m_invertedCovariance = (std::vector <double> *)malloc(_needMallocMemory);
    for (int _row = 0; _row < m_nDimensions; _row++){
        m_invertedCovariance[_row].resize(m_nDimensions);
    }
}

void HamiltonianSampler::StoreMatrixElements(const TMatrixDSym& _invertedMatrix) {
    //Store the matrix as row major to optimise memory access
    for (int _row = 0; _row < m_nDimensions; _row++) {
        for (int _column = 0; _column < m_nDimensions; _column++) {
            m_invertedCovariance[_row][_column] = _invertedMatrix(_row, _column);
        }
    }
}

#endif
