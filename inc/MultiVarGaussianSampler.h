#ifndef MULTIVARGAUSSIANSAMPLER_HPP
#define MULTIVARGAUSSIANSAMPLER_HPP

#include "TMatrixDSym.h"
#include <vector>
#include <stdexcept>
#include <iostream>

class MultiVarGaussianSampler{
public:
    MultiVarGaussianSampler();
    MultiVarGaussianSampler(const std::vector <double> & _means, const TMatrixDSym& _covarianceMatrix);
    virtual std::vector <double> Sample() = 0;

protected:
    void StoreMultiVarGaussian(const std::vector <double> & _means, const TMatrixDSym& _invertedMatrix);

private:
    void ThrowIfDimensionDoesNotAgree(const std::vector <double> & _means, const TMatrixDSym& _covarianceMatrix) const ;
    virtual void SetDimensions(const std::vector <double> & _means) = 0;
    virtual void StoreMeans(const std::vector <double> & _means) = 0;
    virtual void StoreInvertedCovariance(const TMatrixDSym& _invertedMatrix) = 0;
};

class DimensionError : public std::logic_error {
public:
    DimensionError(std::string _nDimMeans, std::string _nDimMatrix) : 
        logic_error("The dimensions of means and covariance matrix passed do not match!. \n nDimMeans : " + _nDimMeans + "\n nDimMatrix : " + _nDimMatrix) { };
};

#endif
