#ifndef HAMILTONIANSAMPLER_HPP
#define HAMILTONIANSAMPLER_HPP

#include "MultiVarGaussianSampler.hpp"
#include "TRandom3.h"

class HamiltonianSampler : public MultiVarGaussianSampler {

public:
    HamiltonianSampler();
    HamiltonianSampler(const std::vector <double> _means, const TMatrixDSym& _covarianceMatrix);
    std::vector <double> Sample();

public:
    static TRandom3 m_numberGenerator;

private:
    enum LeapLength {
        FullLeap = 0,
        HalfLeap = 1
    };

    void StoreErrors(const TMatrixDSym& _covarianceMatrix);
    void AllocateMemory();
    void GetInitialState();
    void CalculatePositionOffset();
    void CalculateDifferentialPotential();
    double GetCurrentHamiltonian();
    double GetCurrentKineticEnergy();
    double GetCurrentPotentialEnergy();
    void RunBurnInCycles();
    double SampleOnce();
    void ProposeNewMomentum();
    void MomentumLeap(LeapLength _leapLength);
    void PositionLeap();
    double GetAcceptanceProbability();
    void SetDimensions(const std::vector <double> & _means);
    void StoreMeans(const std::vector <double> & _means);
    void StoreInvertedCovariance(const TMatrixDSym& _invertedMatrix);
    void AllocateMatrixMemory();
    void StoreMatrixElements(const TMatrixDSym& _invertedMatrix);

private:
    const double m_stepSize = 0.05;
    const int m_leapPerSample = 20;
    const int m_burnInCycles = 100;

    int m_nDimensions;
    std::vector <double> * m_invertedCovariance;
    std::vector <double> m_means;
    std::vector <double> m_errors;

    double m_previousHamiltonian;
    double m_currentHamiltonian;
    std::vector <double> m_previousPosition;
    std::vector <double> m_currentPosition;
    std::vector <double> m_previousMomentum;
    std::vector <double> m_currentMomentum;
    std::vector <double> m_differentialPotential;
    std::vector <double> m_positionOffset;
};

#endif
