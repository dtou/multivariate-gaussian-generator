#ifndef GIBBSSAMPLER_HPP
#define GIBBSSAMPLER_HPP

#include "TRandom3.h"
#include "MultiVarGaussianSampler.hpp"

class GibbsSampler : public MultiVarGaussianSampler{
public:
    GibbsSampler();
    GibbsSampler(const std::vector <double>& means, const TMatrixDSym& covarianceMatrix);
    std::vector <double> Sample();

public:
    static TRandom3 m_generator;
    
private:
    void CacheDeviations();
    void GetInitialState();
    void RunBurnInCycles();
    void SampleOnce();
    double GetConditionalMean(std::vector <double> & _currentState, const int _index);
    void SetDimensions(const std::vector <double> & _means);
    void StoreMeans(const std::vector <double> & _means);
    void StoreInvertedCovariance(const TMatrixDSym& _invertedMatrix);
    void AllocateMatrixMemory();
    void StoreMatrixElements(const TMatrixDSym& _invertedMatrix);
    void StoreDiagonalElements(const TMatrixDSym& _invertedMatrix);
    
private:
    const int m_burnIn = 1000;
    const int m_nSkip = 20;
    int m_nDimensions;
    std::vector <double> m_diagonal;
    std::vector <double> m_means;
    std::vector <double> m_standardDeviations; //This is a constant across all loops
    std::vector <double> m_currentDeviations;
    std::vector <double> m_previousState;
    std::vector <double> * m_invertedCovariance;
};

#endif
